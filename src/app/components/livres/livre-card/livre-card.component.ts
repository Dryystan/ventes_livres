import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Livre } from 'src/app/models/livre';

@Component({
  selector: 'app-livre-card',
  templateUrl: './livre-card.component.html',
  styleUrls: ['./livre-card.component.scss'],
})
export class LivreCardComponent implements OnInit {

  @Input() livre!: Livre;
  @Input() quantite: number = 0;
  @Output() supprimerLivreEvent = new EventEmitter<number>();
  @Output() ajouterLivreEvent = new EventEmitter<number>();
  @Input() deletable: boolean = false;
  @Input() addable: boolean = false;

  constructor() { }

  ngOnInit() {}

  supprimerLivre() {
    if(this.deletable) {
      this.supprimerLivreEvent.emit(this.livre.id);
    }
  }

  ajouterLivre() {
    if(this.addable) {
      this.ajouterLivreEvent.emit(this.livre.id);
    }
  }
}
