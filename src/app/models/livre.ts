export class Livre {
  id: number;
  editeur: string;
  titre: string;
  auteur: string;
  prix: number;

  constructor(id: number, editeur: string, titre: string, auteur: string, prix: number) {
    this.id = id;
    this.editeur = editeur;
    this.titre = titre;
    this.auteur = auteur;
    this.prix = prix;
  }
}
