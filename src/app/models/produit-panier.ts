import { Livre } from './livre';

export class ProduitPanier {
  id: number;
  quantite: number;
  livre: Livre;

  constructor(id: number, quantite: number, livre: Livre) {
    this.id = id;
    this.quantite = quantite;
    this.livre = livre;
  }
}
