import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LivresPageRoutingModule } from './livres-routing.module';

import { LivresPage } from './livres.page';

import { LivreCardComponent } from 'src/app/components/livres/livre-card/livre-card.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LivresPageRoutingModule
  ],
  declarations: [LivresPage, LivreCardComponent]
})
export class LivresPageModule {}
