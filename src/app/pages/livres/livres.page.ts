import { Component, OnInit } from '@angular/core';
import { Livre } from 'src/app/models/livre';
import { ProduitPanier } from 'src/app/models/produit-panier';
import { LivreService } from 'src/app/services/livre.service';
import { PanierService } from 'src/app/services/panier.service';

@Component({
  selector: 'app-livres',
  templateUrl: './livres.page.html',
  styleUrls: ['./livres.page.scss'],
})
export class LivresPage implements OnInit {

  livres: Livre[] = [];

  constructor(private livreService: LivreService,
              private panierService: PanierService) {

  }

  ngOnInit() {
    this.initLivre();
  }

  ionViewDidEnter() {
    this.initLivre();
  }

  initLivre() {
    this.livreService.findAll().subscribe(data => {
      this.livres = [...data];
    })
  }

  ajouterLivre(id: number) {
    this.panierService.findById(id).subscribe(data => {
      data.quantite++;
      this.panierService.update(id, data).subscribe(() => {});
    },
    error => {
      if(error.status == 404) {
        this.livreService.findById(id).subscribe(livre => {
          this.panierService.create(new ProduitPanier(id, 1, livre)).subscribe(() => {});
        });
      }
    })
  }
}
