import { Component, OnInit } from '@angular/core';
import { ProduitPanier } from 'src/app/models/produit-panier';
import { PanierService } from 'src/app/services/panier.service';

@Component({
  selector: 'app-panier',
  templateUrl: './panier.page.html',
  styleUrls: ['./panier.page.scss'],
})
export class PanierPage implements OnInit {

  panier: ProduitPanier[] = [];
  prix: number = 0;

  constructor(private panierService: PanierService) {

  }

  ngOnInit() {
    this.initPanier();
  }

  ionViewWillEnter() {
    this.initPanier();
  }

  initPanier() {
    this.panierService.findAll().subscribe(data => {
      this.panier = [...data];
      this.prix = 0;
      this.panier.forEach(produit => {
        this.prix += produit.livre.prix * produit.quantite;
      });
    })
  }

  supprimerLivre(id: number) {
    this.panierService.findById(id).subscribe(data => {
      if(data.quantite > 1) {
        data.quantite--;
        this.panierService.update(id, data).subscribe(() => {
          this.initPanier();
        })
      }
      else {
        this.panierService.delete(id).subscribe(() => {
          this.initPanier();
        })
      }
    })
  }
}
