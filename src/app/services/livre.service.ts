import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Livre } from '../models/livre';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LivreService {

  constructor(private http: HttpClient) { }

  findAll(): Observable<Livre[]> {
    return this.http.get<Livre[]>('http://localhost:3000/livres');
  }

  findById(id:number): Observable<Livre> {
    return this.http.get<Livre>(`http://localhost:3000/livres/${id}`)
  }

  create(livre: Livre): Observable<Livre> {
    return this.http.post<Livre>('http://localhost:3000/livres', livre);
  }

  delete(id: number): Observable<Livre> {
    return this.http.delete<Livre>(`${environment.apiUrl}/livres/${id}`);
  }

  update(id: number, livre: Livre): Observable<Livre> {
    return this.http.put<Livre>(`http://localhost:3000/livres/${id}`, livre);
  }
}
