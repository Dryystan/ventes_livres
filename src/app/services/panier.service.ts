import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ProduitPanier } from '../models/produit-panier';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PanierService {

  constructor(private http: HttpClient) { }

  findAll(): Observable<ProduitPanier[]> {
    return this.http.get<ProduitPanier[]>('http://localhost:3000/panier');
  }

  findById(id:number): Observable<ProduitPanier> {
    return this.http.get<ProduitPanier>(`http://localhost:3000/panier/${id}`)
  }

  create(produit_panier: ProduitPanier): Observable<ProduitPanier> {
    return this.http.post<ProduitPanier>('http://localhost:3000/panier', produit_panier);
  }

  delete(id: number): Observable<ProduitPanier> {
    return this.http.delete<ProduitPanier>(`${environment.apiUrl}/panier/${id}`);
  }

  update(id: number, produit_panier: ProduitPanier): Observable<ProduitPanier> {
    return this.http.put<ProduitPanier>(`http://localhost:3000/panier/${id}`, produit_panier);
  }
}
